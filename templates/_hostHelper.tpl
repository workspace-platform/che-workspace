{{- define "cheHost" }}
{{- if or (eq .Values.global.serverStrategy "default-host") (eq .Values.global.serverStrategy "single-host") }}
{{- printf "%s" .Values.global.ingressDomain }}
{{- else }}
{{- printf "ide.%s" .Values.global.ingressDomain }}
{{- end }}
{{- end }}
